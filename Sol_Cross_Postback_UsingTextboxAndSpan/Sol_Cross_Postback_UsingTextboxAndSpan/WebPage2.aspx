﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebPage2.aspx.cs" Inherits="Sol_Cross_Postback_UsingTextboxAndSpan.WebPage2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       
        <p>
            <span id="spanName" runat="server"></span>
        </p> 
        <p>
            <span id="spanName1" runat="server"></span>
        </p> 
        <p>
            <span id="spanName2" runat="server"></span>
        </p> 
        <p>
            <span id="spanName3" runat="server"></span>
        </p> 
        <p>
            <span id="spanName4" runat="server"></span>
        </p>       
        
    
    </div>
    </form>
</body>
</html>
